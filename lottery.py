import random

def conduct_lottery(applicants, number):
    """
        Randomly select {number} of applicants from pool of {applicants} list
    """
    if number > len(applicants):
        raise Exception("Number of applicants to select exceeds the number of applicants in the pool")
    return random.sample(applicants, number)